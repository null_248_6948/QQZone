//
//  Common.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//


import UIKit

// MARK:- 定义常量
let kScreenW = UIScreen.main.bounds.width
let kScreenH = UIScreen.main.bounds.height

let kDockLandscapeW : CGFloat = 270
let kDockPortraitW : CGFloat = 70

let kDockItemH : CGFloat = 70


let kIconBtnY : CGFloat = 60
let kIconBtnPortraitWH : CGFloat = 60

let kIconBtnLandscapeW : CGFloat = 90
let kIconBtnLandscapeH : CGFloat = 120

