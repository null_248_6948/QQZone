//
//  TabbarItem.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//


import UIKit

private let kRatio : CGFloat = 0.4

class TabbarItem: UIButton {
    
    // MARK: 点击去除高亮调整
    override var isHighlighted: Bool {
        didSet {
            super.isHighlighted = false
        }
    }
    
    
    // MARK: 构造函数
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView?.contentMode = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: 布局子控件
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if bounds.width != bounds.height {
            imageView?.frame = CGRect(x: 0, y: 0, width: frame.width * kRatio, height: frame.height)
            titleLabel?.frame = CGRect(x: imageView!.frame.maxX, y: 0, width: frame.width * (1 - kRatio), height: frame.height)
        } else {
            imageView?.frame = bounds
            titleLabel?.frame = CGRect.zero
        }
    }
}
