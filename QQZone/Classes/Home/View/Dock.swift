//
//  Dock.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class Dock: UIView {
    lazy var bottomMenu : BottomMenu = BottomMenu()
    lazy var tabbar : Tabbar = Tabbar()
    lazy var iconBtn : IconButton = IconButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK:- 设置UI界面
extension Dock {
    fileprivate func setupUI() {
        addSubview(bottomMenu)
        addSubview(tabbar)
        addSubview(iconBtn)
    }
}


// MARK:- 对外提供的函数
extension Dock {
    func setupCurrentOritation(_ isLandscape : Bool) {
        bottomMenu.setupCurrentOritation(isLandscape)
        tabbar.setupCurrentOritation(isLandscape)
        tabbar.frame.origin.y = frame.height - bottomMenu.frame.height - tabbar.frame.height
        iconBtn.setupCurrentOritation(isLandscape)
    }
}
