//
//  IconButton.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//


import UIKit

class IconButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setTitle("hjj哥", for: .normal)
        setImage(UIImage(named: "Easy"), for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
        titleLabel?.textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if bounds.width != bounds.height { // 横屏
            imageView?.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.width)
            titleLabel?.frame = CGRect(x: 0, y: imageView!.frame.maxY, width: bounds.width, height: bounds.height - bounds.width)
        } else { // 竖屏
            imageView?.frame = bounds
            titleLabel?.frame = CGRect.zero
        }
    }
}

// MARK:- 对外提供的函数
extension IconButton {
    func setupCurrentOritation(_ isLandscape : Bool) {
        // 1.调整自身的frame
        let w : CGFloat = isLandscape ? kIconBtnLandscapeW : kIconBtnPortraitWH
        let h : CGFloat = isLandscape ? kIconBtnLandscapeH : kIconBtnPortraitWH
        let x : CGFloat = (superview!.frame.width - w) * 0.5
        let y : CGFloat = kIconBtnY
        frame = CGRect(x: x, y: y, width: w, height: h)
    }
}
