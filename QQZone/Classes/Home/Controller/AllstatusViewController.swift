//
//  AllstatusViewController.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//


import UIKit

class AllstatusViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.purple
        
        let sc = UISegmentedControl(items: ["全部", "特别关心", "好友动态", "认证空间"])
        sc.tintColor = UIColor.lightGray
        sc.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.black], for: .normal)
        sc.selectedSegmentIndex = 0
        navigationItem.titleView = sc
        sc.addTarget(self, action: #selector(scClick(_:)), for: .valueChanged)
    }
    
    @objc private func scClick(_ sc : UISegmentedControl) {
        print(sc.selectedSegmentIndex)
    }
}
