//
//  HomeViewController.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    // MARK: 属性
    fileprivate lazy var dock : Dock = Dock()
    fileprivate lazy var containerView : UIView = UIView()
    fileprivate var currentIndex : Int = 0
    
    // MARK: 系统回调函数
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupControllers()
        iconBtnClick()
    }
}


// MARK:- 设置UI界面
extension HomeViewController {
    fileprivate func setupUI() {
        // 1.设置控制器的View的背景
        view.backgroundColor = UIColor(r: 55, g: 55, b: 55)
        
        // 2.添加Dock
        setupDock()
        
        // 3.添加容器的View
        setupContainerView()
        
        // 4.监听dock内部BottomMenu的点击
        dock.bottomMenu.itemClickCallback = {[weak self] (type : BottomMenuType) in
            switch type {
            case .mood:
                let moodVc = MoodViewController()
                let moodNav = UINavigationController(rootViewController: moodVc)
                // formSheet : 在iPad中,以屏幕的中间部分来呈现控制器
                moodNav.modalPresentationStyle = .formSheet
                moodNav.modalTransitionStyle = .flipHorizontal
                self?.present(moodNav, animated: true, completion: nil)
            case .photo:
                print("点击照片")
            case .blog:
                print("点击日志")
            }
        }
        
        // 5.监听tabbar的点击
        dock.tabbar.delegate = self
        
        // 6.监听Button的点击
        dock.iconBtn.addTarget(self, action: #selector(iconBtnClick), for: .touchUpInside)
    }
    
    private func setupDock() {
        // 1.设置Dock的frame
        let isLandscape = kScreenW > kScreenH
        let dockW = isLandscape ? kDockLandscapeW : kDockPortraitW
        dock.frame = CGRect(x: 0, y: 0, width: dockW, height: view.frame.height)
        dock.setupCurrentOritation(isLandscape)
        
        // 2.将dock添加到控制器的View中
        view.addSubview(dock)
    }
    
    private func setupContainerView() {
        let width = min(kScreenH, kScreenW) - kDockPortraitW
        containerView.frame = CGRect(x: dock.frame.maxX, y: 20, width: width, height: view.frame.height - 20)
        containerView.backgroundColor = UIColor.yellow
        
        // 2.将containerView添加到控制器的View中
        view.addSubview(containerView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}


// MARK:- 初始化所有的控制器
extension HomeViewController {
    fileprivate func setupControllers() {
        // 全部动态
        addChildVc(AllstatusViewController(), title: "全部动态", bgColor: UIColor.green)
        addChildVc(UIViewController(), title: "与我相关", bgColor: UIColor.white)
        addChildVc(UIViewController(), title: "照片墙", bgColor: UIColor.lightGray)
        addChildVc(UIViewController(), title: "电子相框", bgColor: UIColor.orange)
        addChildVc(UIViewController(), title: "好友", bgColor: UIColor.blue)
        addChildVc(UIViewController(), title: "更多", bgColor: UIColor.purple)
        addChildVc(UIViewController(), title: "个人中心", bgColor: UIColor.black)
    }
    
    private func addChildVc(_ vc : UIViewController, title : String, bgColor : UIColor) {
        vc.title = title
        vc.view.backgroundColor = bgColor
        let nav = UINavigationController(rootViewController: vc)
        addChildViewController(nav)
    }
}


// MARK:- 监听屏幕的旋转
extension HomeViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        UIView.animate(withDuration: coordinator.transitionDuration, animations: {
            let isLandscape = size.width > size.height
            self.dock.frame.size.width = isLandscape ? kDockLandscapeW : kDockPortraitW
            self.dock.frame.size.height = size.height
            self.dock.setupCurrentOritation(isLandscape)
            
            self.containerView.frame.origin.x = self.dock.frame.maxX
            self.containerView.frame.size.height = size.height - 20
        })
    }
}


// MARK:- 遵守TabbarDelegate协议
extension HomeViewController : TabbarDelegate {
    func tabbar(tabbar: Tabbar, toIndex: Int, fromIndex: Int) {
        // 1.取出旧的控制器的View
        let oldVc = childViewControllers[fromIndex]
        oldVc.view.removeFromSuperview()
        
        // 2.取出新的控制器的View
        let newVc = childViewControllers[toIndex]
        containerView.addSubview(newVc.view)
        newVc.view.frame = containerView.bounds
        
        // 3.记录当前选中的下标志
        currentIndex = toIndex
    }
}



// MARK:- 监听IconButton的点击
extension HomeViewController {
    @objc fileprivate func iconBtnClick() {
        tabbar(tabbar: dock.tabbar, toIndex: childViewControllers.count - 1, fromIndex: currentIndex)
        dock.tabbar.selectedItem?.isSelected = false
    }
}
