//
//  MoodViewController.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//


import UIKit

class MoodViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "发表心情"
        view.backgroundColor = UIColor.purple
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "退出", style: .plain, target: self, action: #selector(exitItemClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "发布", style: .plain, target: nil, action: nil)
    }
    
    @objc private func exitItemClick() {
        dismiss(animated: true, completion: nil)
    }
}
