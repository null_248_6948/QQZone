//
//  LoginViewController.swift
//  QQZone
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: 控件属性
    @IBOutlet weak var accountField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var remPasswordBtn: UIButton!
    @IBOutlet weak var autoLoginBtn: UIButton!
    @IBOutlet weak var loginView: UIView!
    
    // MARK: 系统回调函数
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


// MARK:- 自动登录&记住密码的逻辑
extension LoginViewController {
    @IBAction func remPasswordBtnClick(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        
        if !sender.isSelected {
            autoLoginBtn.isSelected = false
        }
    }
    
    
    @IBAction func autoLoginBtnClick(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            remPasswordBtn.isSelected = true
        }
    }
}


// MARK:- 遵守UITextFieldDelegate,并且实现代理方法
extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == accountField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            loginBtnClick()
        }
        
        return true
    }
}


// MARK:- 登录功能的实现
extension LoginViewController {
    @IBAction func loginBtnClick() {
        // 1.退出键盘
        view.endEditing(true)
        
        // 2.判断账号是否有输入内容
        let account = accountField.text!
        if account.characters.count == 0 {
            showErrorInfo("账号不能为空")
            return
        }
        
        // 3.判断密码是否有输入内容
        let password = passwordField.text!
        if password.characters.count == 0 {
            showErrorInfo("密码不能为空")
            return
        }
        
        // 4.判断账号&密码是否正确
        if account == "123" && password == "123" {
            let homeVc = HomeViewController()
            present(homeVc, animated: false, completion: nil)
        } else {
            showErrorInfo("账号或密码错误")
        }
    }
    
    private func showErrorInfo(_ errorInfo : String) {
        // 1.弹出提示的弹窗
        let alertVc = UIAlertController(title: "登录失败", message: errorInfo, preferredStyle: .alert)
        alertVc.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
        present(alertVc, animated: true, completion: nil)
        
        // 2.整体登录的View需要一个摇晃的动画
        // 2.1.创建动画(CABasicAnimation/CAKeyframeAnimation)
        let shakeAnim = CAKeyframeAnimation(keyPath: "transform.translation.x")
        
        // 2.2.给动画设置属性
        shakeAnim.values = [0, -10, 0, 10, 0]
        shakeAnim.duration = 0.2
        shakeAnim.repeatCount = 3
        
        // 2.3.将动画添加到对应View的layer中
        loginView.layer.add(shakeAnim, forKey: nil)
    }
}
